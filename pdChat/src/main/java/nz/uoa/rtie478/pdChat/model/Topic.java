package nz.uoa.rtie478.pdChat.model;

import java.util.Collections;
import java.util.List;

import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import pdstore.GUID;

public class Topic {
	private String id;
	private transient GUID guid;
	private String name;
	private String attachment;
	private boolean subscribed = false; //If there is an authenticated user with the request, then this is set to the users subscription status.
	private long creation_timestamp;
	
	private transient GUID groupGuid;
	private String groupId;
	private Group group;
	
	private List<User> members;
	private List<Message> messages;
	
	
	
	public void setSubscribed(boolean subed) {
		this.subscribed = subed;
	}
	
	public boolean getSubscribed() {
		return this.subscribed;
	}
	
	/**
	 * Populate this topics member list
	 */
	public void populateMembers() {
		this.setMembers(ChatStore.getInstance().getTopicMembers(this.guid));
	}
	
	/*
	 * Populate this topics messages list
	 */
	public void populateMessages() {
		List<Message> messages = ChatStore.getInstance().getTopicMessages(this.guid);
		//TODO, change the order of the messages. Current it is newest first, we want newest last in the array.
		Collections.reverse(messages);
		this.setMessages(messages);
	}
	
	public void setGuid(GUID guid) {
		this.guid = guid;
		this.id = GUID.toHexString(this.guid.toByteArray());
		this.creation_timestamp = this.guid.time();
	}
	
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public List<Message> getMessages() {
		return messages;
	}
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	public List<User> getMembers() {
		if(this.members == null) {
			this.populateMembers();
		}
		return members;
	}
	public void setMembers(List<User> members) {
		this.members = members;
	}
	
	public String getId() {
		return id;
	}

	public GUID getGuid() {
		return guid;
	}
	
	public long getCreationTimestamp() {
		return creation_timestamp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public Group getGroup() {
		if(this.group == null) {
			if(this.groupId != null) {
				this.group = Group.fromHex(this.groupId);
			}
			else if(this.groupGuid != null) {
				this.group = Group.fromGUID(this.groupGuid);
			}
		}
		return this.group;
	}
	
	public GUID getGroupGuid() {
		return groupGuid;
	}

	public void setGroupGuid(GUID groupGuid) {
		this.groupGuid = groupGuid;
		this.groupId = GUID.toHexString(this.groupGuid.toByteArray());
	}

	public String getGroupId() {
		return groupId;
	}

	public static Topic fromGUID(GUID topicGUID, boolean loadDetails) {
		Topic t;
		if(loadDetails) {
			t = ChatStore.getInstance().loadTopic(topicGUID);
		}else {
			t = new Topic();
			t.setGuid(topicGUID);
		}
		
		
		return t;
	}
	public static Topic fromGUID(GUID topicGUID) {
		return Topic.fromGUID(topicGUID, true);
	}
	public static Topic fromHex(String hex, boolean loadDetails) {
		GUID topicGUID = new GUID(hex);
		return Topic.fromGUID(topicGUID, loadDetails);
	}
	public static Topic fromHex(String hex) {
		return Topic.fromHex(hex, true);
	}

}
