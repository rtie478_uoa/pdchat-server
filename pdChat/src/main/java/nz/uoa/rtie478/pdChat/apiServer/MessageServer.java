package nz.uoa.rtie478.pdChat.apiServer;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import nz.uoa.rtie478.pdChat.model.Message;
import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import nz.uoa.rtie478.pdChat.websocket.WebsocketServer;

@Path("message")
public class MessageServer {
	Gson gson = new Gson();
	@GET
	@Path("/{messageid}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getMessage(@PathParam(value = "messageid") String messageHex) {
		Message m = Message.fromHex(messageHex);
		return gson.toJson(m);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendMessage(String json) {
		try {
			Message m = gson.fromJson(json, Message.class);
			ChatStore.getInstance().insertMessage(m);
			WebsocketServer.distribute(m);
		
			return gson.toJson(m);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "WHAT";
	}
}
