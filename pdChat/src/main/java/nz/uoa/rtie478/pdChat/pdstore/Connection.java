package nz.uoa.rtie478.pdChat.pdstore;

import pdstore.PDStore;

public class Connection {
	private static PDStore instance;
	public static PDStore getInstance() {
		if(Connection.instance == null) {
			Connection.instance = new PDStore("pdChat");
		}
		return Connection.instance;
	}
	
	public static PDStore newInstance(String storeName) {
		Connection.instance = new PDStore(storeName);
		return Connection.instance;
	}
	
}
