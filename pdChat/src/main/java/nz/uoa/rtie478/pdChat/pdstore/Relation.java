package nz.uoa.rtie478.pdChat.pdstore;

import pdstore.GUID;

public enum Relation {
	/* Relations defined for common two way interactions */
	GroupTopicRelation (new GUID(0xe215b6b555a5L, 0x358e651600b150L)),
	TopicMessageRelation (new GUID(0xe215b6b555a5L, 0x358e65163507c0L));
	
	private GUID relation;
	Relation(GUID r){
		this.relation = r;
	}
	public GUID getRelation() {
		return this.relation;
	}
	
	/* Sub Types (used for creating the relationships) */
	enum Message{
		hasContent (new GUID(0xe215b6b555a5L, 0x358e65163507c2L)),
		hasSender (new GUID(0xe215b6b555a5L, 0x358e6516352ed0L)),
		hasTopic (new GUID(0xe215b6b555a5L, 0x358e6516352ed2L));
		private GUID relation;
		Message(GUID r){
			this.relation = r;
		}
		public GUID get() {
			return this.relation;
		}
	}
	
	enum User{
		hasName (new GUID(0xe215b6b555a5L, 0x358e6516352ed4L)),
		hasUsername (new GUID(0xe215b6b555a5L, 0x358e6516352ed6L)),
		subscribedTo (new GUID(0xe215b6b555a5L, 0x358e6516352ed8L)),
		memberOf (new GUID(0xe215b6b555a5L, 0x358e6516352edaL)),
		hasStatus (new GUID(0xe215b6b555a5L, 0x358e6516352edcL));
		private GUID relation;
		User(GUID r) {
			this.relation = r;
		}
		public GUID get() {
			return this.relation;
		}
	}
	
	enum Topic{
		hasName (new GUID(0xe215b6b555a5L, 0x358e65163555e0L)),
		hasGroup (new GUID(0xe215b6b555a5L, 0x358e65163555e2L)),
		hasAttachment (new GUID(0xe215b6b555a5L, 0x358e65163555e4L)),
		hasSubscribee  (new GUID(0xe215b6b555a5L, 0x358e65163555e6L)),
		hasMessage  (new GUID(0xe215b6b555a5L, 0x358e65163555e8L));
		
		private GUID relation;
		Topic(GUID r){
			this.relation = r;
		}
		public GUID get() {
			return this.relation;
		}
	}
	
	enum Group{
		hasName (new GUID(0xe215b6b555a5L, 0x358e6516357cf0L)),
		hasTopic (new GUID(0xe215b6b555a5L, 0x358e6516357cf2L)),
		hasMemeber (new GUID(0xe215b6b555a5L, 0x358e6516357cf4L));
		
		private GUID relation;
		Group(GUID r){
			this.relation = r;
		}
		public GUID get() {
			return this.relation;
		}
	}
	
	enum Status{
		hasText (new GUID(0x94de80bf4864L, 0x35905a478d3e40L)),
		hasMood (new GUID(0x94de80bf4864L, 0x35905a47b5fbf0L)),
		hasGroup (new GUID(0x94de80bf4864L, 0x35905c342fbec0L)),
		hasTopic (new GUID(0x94de80bf4864L, 0x35905c3458ca90L));
		private GUID relation;
		Status(GUID r){
			this.relation = r;
		}
		public GUID get() {
			return this.relation;
		}
	}
}
