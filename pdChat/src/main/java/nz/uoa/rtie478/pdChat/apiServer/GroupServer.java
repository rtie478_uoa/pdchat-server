package nz.uoa.rtie478.pdChat.apiServer;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import nz.uoa.rtie478.pdChat.model.Group;
import nz.uoa.rtie478.pdChat.model.Message;
import nz.uoa.rtie478.pdChat.model.Topic;
import nz.uoa.rtie478.pdChat.model.User;
import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import pdstore.GUID;

@Path("group")
public class GroupServer {
	Gson gson = new Gson();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getGroups(@Context ContainerRequestContext context) {
		List<Group> groups = ChatStore.getInstance().allGroups();
		return gson.toJson(groups);
	}
	
	@GET
	@Path("/{groupip}")
	@Produces(MediaType.APPLICATION_JSON)
	public String loadGroup(@PathParam(value = "groupip") String groupip) {
		try {
			Group group = Group.fromHex(groupip);
			group.populateTopics();
			group.populateMembers();
			return gson.toJson(group);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String newGroup(String json, @Context ContainerRequestContext context) {
		User authenticatedUser = AuthFilter.getUser(context);
		
		Group inputGroup = gson.fromJson(json, Group.class);
		GUID newGroup = ChatStore.getInstance().newGroup(inputGroup.getName());
		Group g = ChatStore.getInstance().loadGroup(newGroup);
		
		//Add the creator to the new group.
		ChatStore.getInstance().joinGroup(authenticatedUser.getGuid(), g.getGuid());
		
		//Create the general topic.
		GUID newTopic = ChatStore.getInstance().newTopic(g.getGuid(), "General", "http://pdchat.xyz:8040/group/" + g.getId());
		ChatStore.getInstance().subscribeToTopic(authenticatedUser.getGuid(), newTopic);
		
		Topic topic = Topic.fromGUID(newTopic);
		
		//Insert creation meessage
		Message creationMessage = new Message();
		creationMessage.setSenderUser(authenticatedUser);
		creationMessage.setContent("Created this topic!");
		creationMessage.setTopicId(topic.getId());
		ChatStore.getInstance().insertMessage(creationMessage);
		
		return gson.toJson(g);
	}

	@POST
	@Path("/{groupid}/add/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public String joinGroup(@PathParam(value = "groupid") String groupid, @PathParam(value = "userid") String userid) {
		GUID userID = new GUID(userid);
		GUID groupID = new GUID(groupid);
		boolean result = ChatStore.getInstance().joinGroup(userID, groupID);
		return gson.toJson(result);
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{groupid}/remove/{userid}")
	public String leaveGroup(@PathParam(value = "groupid") String groupid, @PathParam(value = "userid") String userid) {
		GUID user = new GUID(userid);
		GUID group = new GUID(groupid);
		boolean result = ChatStore.getInstance().leaveGroup(user, group);
		return gson.toJson(result);
	
	}
}
