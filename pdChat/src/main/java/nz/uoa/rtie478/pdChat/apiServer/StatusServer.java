package nz.uoa.rtie478.pdChat.apiServer;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import nz.uoa.rtie478.pdChat.model.Status;
import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import pdstore.GUID;

@Path("status")
public class StatusServer {
	Gson gson = new Gson();

	/**
	 * Returns the status of the current user.
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllStatus() {
		List<Status> status = ChatStore.getInstance().allStatus();
		return gson.toJson(status);
	}
	
	@GET
	@Path("/foruser/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllStatusForUser(@PathParam(value = "userid") String userid) {
		List<Status> status = ChatStore.getInstance().allStatusForUser(new GUID(userid));
		return gson.toJson(status);
	}
	
	
}
