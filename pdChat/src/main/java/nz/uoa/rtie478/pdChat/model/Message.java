package nz.uoa.rtie478.pdChat.model;

import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import pdstore.GUID;

public class Message {
	private transient GUID guid;
	private String id;
	private long timestamp;
	private String content;
	
	private transient GUID senderGUID;
	private String sender_id;
	private User sender;
	
	private transient GUID topicGUID;
	private Topic topic;
	private String topic_id;
	
	/**
	 * Loads the messages details from the chatStore. 
	 */
	public void loadDetails() {
		ChatStore.getInstance().loadMessage(this);
	}
	
	public static Message fromGUID(GUID messageGUID, boolean loadDetails) {
		Message m = new Message();
		m.guid = messageGUID;
		m.id = GUID.toHexString(m.guid.toByteArray());
		m.timestamp = m.guid.time();
		if(loadDetails) {
			m.loadDetails();
		}
		return m;
	}
	
	public Topic getTopic() {
		if(this.topic == null) {
			if(this.topic_id != null) {
				this.topic = Topic.fromHex(this.topic_id);
			}
			else if(this.topicGUID != null) {
				this.topic = Topic.fromGUID(this.topicGUID);
			}
		}
		return this.topic;
	}
	
	
	public static Message fromGUID(GUID messageGUID) {
		return Message.fromGUID(messageGUID, true);
	}
	public static Message fromHex(String hex, boolean loadDetails) {
		GUID messageGUID = new GUID(hex);
		return Message.fromGUID(messageGUID, loadDetails);
	}
	public static Message fromHex(String hex) {
		return Message.fromHex(hex, true);
	}
	
	public Message insertSelf() {
		GUID messageGUID = ChatStore.getInstance().insertMessage(this);
		this.setGuid(messageGUID);
		return this;
	}

	public GUID getGuid() {
		return guid;
	}

	public void setGuid(GUID guid, boolean updateId) {
		this.guid = guid;
		if(updateId) {
			this.id = GUID.toHexString(this.guid.toByteArray());
		}
		this.timestamp = this.guid.time();
	}
	
	public void setGuid(GUID guid) {
		this.setGuid(guid, true);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getSenderUser() {
		if(this.sender == null && this.sender_id != null) {
			this.sender = User.fromHex(this.sender_id);
		}
		return this.sender;
	}
	
	public void setSenderUser(User sender) {
		this.sender_id = sender.getId();
		this.senderGUID = sender.getGuid();
		this.sender = sender;
	}
	
	public GUID getSender() {
		return senderGUID;
	}

	public void setSender(GUID sender) {
		this.senderGUID = sender;
		this.sender_id = GUID.toHexString(this.guid.toByteArray());
	}

	public String getSenderId() {
		return sender_id;
	}

	public void setSenderId(String sender_id) {
		this.sender_id = sender_id;
	}


	public GUID getTopicGUID() {
		if(this.topic == null && this.topic == null && this.topic_id != null) {
			this.topic = Topic.fromHex(this.topic_id);
		}
		return this.topic.getGuid();
	}

	public void setTopicGUID(GUID topic) {
		this.topicGUID = topic;
	}

	public String getTopicId() {
		return topic_id;
	}

	public void setTopicId(String topic_id) {
		this.topic_id = topic_id;
	}

	
	

}
