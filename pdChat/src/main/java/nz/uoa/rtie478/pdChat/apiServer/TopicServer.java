package nz.uoa.rtie478.pdChat.apiServer;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import nz.uoa.rtie478.pdChat.model.Message;
import nz.uoa.rtie478.pdChat.model.Topic;
import nz.uoa.rtie478.pdChat.model.User;
import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import nz.uoa.rtie478.pdChat.websocket.WebsocketServer;
import pdstore.GUID;

@Path("topic")
public class TopicServer {
	Gson gson = new Gson();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getTopics() {
		List<Topic> topics = ChatStore.getInstance().allTopics();
		System.out.println("Loading All topics");
		topics.forEach(topic -> {
			topic.getGroup();
		});
		return gson.toJson(topics);
	}
	
	@GET
	@Path("/{topicid}")
	@Produces(MediaType.APPLICATION_JSON)
	public String loadTopic(@PathParam(value = "topicid") String topicid, @Context ContainerRequestContext context) {
		try {
			User authenticatedUser = AuthFilter.getUser(context);
			
			Topic topic = Topic.fromHex(topicid);
			topic.populateMembers();
			topic.populateMessages();
			topic.getGroup();
			
			//tell the socket server that the autenticated user loaded the topic, this lets them recieve notifications for the topic even if they are not subscribed to it.
			WebsocketServer.userLoadedTopic(authenticatedUser, topic);
			
			boolean authenticatedUserIsSubscribed = topic.getMembers().stream().anyMatch((User member) -> {
				return member.getId().equals(authenticatedUser.getId());
			});
			
			
			ArrayList<String> currentlyAddedUIDList = new ArrayList<String>();
			
			//For all the members of this topic, load their status and set them as subscribed members of the topic.
			topic.getMembers().forEach(member -> {
				member.setSubscribed(true);
				currentlyAddedUIDList.add(member.getId());
			});
			
			
			//Add the guest topic viewers to the list of topic members, but dont set them as subscribed members.
			List<User> guestUsers = WebsocketServer.getGuestTopicViewerUsers(topicid);
			guestUsers.forEach(guest -> {
				if(!currentlyAddedUIDList.contains(guest.getId())) {
					topic.getMembers().add(guest);
					currentlyAddedUIDList.add(guest.getId());
				}
			});
			
			//Add any member who has their status set to this topic.
			List<User> statusUsers = ChatStore.getInstance().getMembersWithStatusTopic(topic.getGuid());
			statusUsers.forEach(user ->{
				if(!currentlyAddedUIDList.contains(user.getId())) {
					topic.getMembers().add(user);
					currentlyAddedUIDList.add(user.getId());
				}
			});
			
			//For all the collated users, load their statuses.
			topic.getMembers().forEach(user -> {
				user.loadStatus();
			});
			
			//Is the current authenticated user actually a subscribed member of this topic?
			topic.setSubscribed(authenticatedUserIsSubscribed);
			
			
			
			return gson.toJson(topic);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String newTopic(String json, @Context ContainerRequestContext context) {
		User authenticatedUser = AuthFilter.getUser(context);
		
		Topic inputTopic = gson.fromJson(json, Topic.class);
		GUID newTopicId = ChatStore.getInstance().newTopic(new GUID(inputTopic.getGroupId()), inputTopic.getName(), inputTopic.getAttachment());
		Topic t = ChatStore.getInstance().loadTopic(newTopicId);
		
		//Subscribe creator to the topic automatically.
		ChatStore.getInstance().subscribeToTopic(authenticatedUser.getGuid(), t.getGuid());
		
		//Insert creation meessage
		Message creationMessage = new Message();
		creationMessage.setSenderUser(authenticatedUser);
		creationMessage.setContent("Created this topic!");
		creationMessage.setTopicId(t.getId());
		ChatStore.getInstance().insertMessage(creationMessage);
		
		return gson.toJson(t);
	}

	@POST
	@Path("/{topicid}/subscribe/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public String subscribeToTopic(@PathParam(value = "topicid") String topicid, @PathParam(value = "userid") String userid) {
		GUID userID = new GUID(userid);
		GUID topicID = new GUID(topicid);
		boolean result = ChatStore.getInstance().subscribeToTopic(userID, topicID);
		return gson.toJson(result);
	}
	
	@DELETE
	@Path("/{topicid}/unsubscribe/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public String unsubscribeFromTopic(@PathParam(value = "topicid") String topicid, @PathParam(value = "userid") String userid) {
		GUID user = new GUID(userid);
		GUID topic = new GUID(topicid);
		boolean result = ChatStore.getInstance().unsubscribeFromTopic(user, topic);
		return gson.toJson(result);
	}
}
