package nz.uoa.rtie478.pdChat.websocket;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import com.google.gson.Gson;
import nz.uoa.rtie478.pdChat.model.Message;

public class MessageEncoder implements Encoder.Text<Message> {
	 
    private static Gson gson = new Gson();
 
    public String encode(Message message) throws EncodeException {
        return gson.toJson(message);
    }
 
    public void init(EndpointConfig endpointConfig) {
        // Custom initialization logic
    }
 
    public void destroy() {
        // Close resources
    }
}

