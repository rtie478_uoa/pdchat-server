package nz.uoa.rtie478.pdChat.websocket;

import javax.websocket.Session;

import nz.uoa.rtie478.pdChat.model.Topic;
import nz.uoa.rtie478.pdChat.model.User;

public class SocketUser {
	
	public SocketUser(String sessionid, WebsocketServer session, User u) {
		this.sessionid = sessionid;
		this.session = session;
		this.user = u;
	}
	
	public String sessionid;
	
	public WebsocketServer session;
	
	private User user;
	private Topic currentTopic;
	
	public User getUser() {
		return this.user;
	}
	
	public Topic getCurrentTopic() {
		return this.currentTopic;
	}
	
	public void setCurrentTopic(Topic t) {
		this.currentTopic = t;
	}
}
