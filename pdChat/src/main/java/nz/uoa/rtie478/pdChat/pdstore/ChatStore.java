package nz.uoa.rtie478.pdChat.pdstore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import nz.uoa.rtie478.pdChat.model.Group;
import nz.uoa.rtie478.pdChat.model.Message;
import nz.uoa.rtie478.pdChat.model.Status;
import nz.uoa.rtie478.pdChat.model.Topic;
import nz.uoa.rtie478.pdChat.model.User;
import pdstore.ChangeType;
import pdstore.GUID;
import pdstore.PDChange;
import pdstore.PDStore;
import pdstore.sparql.Assignment;
import pdstore.sparql.Query;
import pdstore.sparql.Variable;
import scala.collection.Iterator;

public class ChatStore {
	private static ChatStore instance;
	private static PDStore store = Connection.getInstance();
	
	/**
	 * Get singleton instance
	 * @return
	 */
	public static ChatStore getInstance() {
		if(ChatStore.instance == null) {
			ChatStore.instance = new ChatStore();
		}
		return ChatStore.instance;
	}
	
	public Status getUserStatus(GUID user) {
		try {
			Iterator<Object> stauses = store.get(user, Relation.User.hasStatus.get());
			GUID userStatus = (GUID) stauses.next();
			return this.loadStatus(userStatus);
		}
		catch(NoSuchElementException e) {
			System.out.println("User has no status");
		}
		Status s = new Status();
		s.setStatus("Offline");
		return s;
	}
	
	public boolean setUserStatus(GUID u, Status s) {
		//First need to delete the existing status relationship.

		
		
		this.removeAllUserStatus(User.fromGUID(u, false));
		
		System.out.println("Updating user status");
		
		GUID t = store.begin();
		GUID status = new GUID();
		
		store.addLink(status, Relation.Status.hasText.get(), s.getStatus());
		store.addLink(status, Relation.Status.hasMood.get(), s.getMood());
		
		Group group = s.getGroup();
		if(group != null) {
			store.addLink(status, Relation.Status.hasGroup.get(), group.getGuid());
		}
		
		Topic topic = s.getTopic();
		if(topic != null) {
			store.addLink(status, Relation.Status.hasTopic.get(), topic.getGuid());
		}
		
		//User & Status
		store.addLink(u, Relation.User.hasStatus.get(), status);
		store.commit();
		return true; //TODO, check if successful
	}
	
	public boolean removeAllUserStatus(User u) {
		GUID t2 = store.begin();

		Collection<Object> status = store.getInstances(u.getGuid(), Relation.User.hasStatus.get());
		status.forEach( s -> {
			store.removeLink(t2, u.getGuid(), Relation.User.hasStatus.get(), (GUID) s);
		});
		store.commit(t2);
		return true;
	}
	
	/**
	 * Load a status
	 * @param status
	 * @return
	 */
	public Status loadStatus(GUID status) {
		Status s = new Status();
		s.setGuid(status);
		
		GUID t = store.begin();
		
		Iterator<Object> status_text = store.get(status, Relation.Status.hasText.get());
		Iterator<Object> status_mood = store.get(status, Relation.Status.hasMood.get());
		Iterator<Object> satus_group = store.get(status, Relation.Status.hasGroup.get());
		Iterator<Object> status_topic = store.get(status, Relation.Status.hasTopic.get());
		
		if(status_text.hasNext()) {
			s.setStatus((String) status_text.next());
		}
		
		if(status_mood.hasNext()) {
			s.setMood((String) status_mood.next());
		}
		
		if(satus_group.hasNext()) {
			s.setGroup((GUID) satus_group.next());
		}
		
		if(status_topic.hasNext()) {
			s.setTopic((GUID) status_topic.next());
		}
		return s;
	}
	
	public List<User> getMembersWithStatusTopic(GUID topic){
		ArrayList<User> statusUsers = new ArrayList<User>();
		
		GUID t = store.begin();
		
		Query q = new Query(
				new ArrayList<Variable>(), 
				new ArrayList<PDChange<GUID, Object, GUID>>(),
				store
			);
			
		Variable user = new Variable("user");
		Variable status = new Variable("status");
		
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, user, Relation.User.hasStatus.get(), status);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, status, Relation.Status.hasTopic.get(), topic);
		
		q.where.add(w1);
		q.where.add(w2);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				User u = User.fromGUID((GUID) result.get(user));
				statusUsers.add(u);
			}catch(Exception e) {
				System.out.println("Failed to convert status user details to user object");
				e.printStackTrace();
			}
		}
		
		return statusUsers;
	}
	
	/**
	 * Populate a message with details from the ChatStore
	 * @param m The message to load the details into
	 */
	public void loadMessage(Message m) {
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable messageContent = new Variable("messageContent");
		Variable sender = new Variable("sender");
		Variable topic = new Variable("topic");
		
		Variable senderName = new Variable("senderName");
		
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, m.getGuid(), Relation.Message.hasContent.get(), messageContent);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, m.getGuid(), Relation.Message.hasSender.get(), sender);
		PDChange<GUID, Object, GUID> w4 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, m.getGuid(), Relation.Message.hasTopic.get(), topic);
		
		q.where.add(w1);
		q.where.add(w2);
		q.where.add(w4);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		if(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				m.setTopicGUID((GUID) result.get(topic));
				m.setTopicId(GUID.toHexString(m.getTopicGUID().toByteArray()));
				
				m.setContent((String) result.get(messageContent));
				
				m.setSender((GUID) result.get(sender));
				m.setSenderId(GUID.toHexString(m.getSender().toByteArray()));
				
				m.setSenderUser(User.fromGUID(m.getSender()));

			}catch(Exception e) {
				System.out.println("Failed to convert message details to message");
				e.printStackTrace();
			}
		}
		//Done
	}
	
	/**
	 * Insert Message into database
	 * @param m The message to insert
	 * @return GUILD of newly inserted message.
	 */
	public GUID insertMessage(Message m) {
		GUID t = store.begin();
		
		GUID newMessage = new GUID();
		store.addLink(t, newMessage, Relation.Message.hasContent.get(), m.getContent());
		store.addLink(t, newMessage, Relation.Message.hasSender.get(), m.getSenderUser().getGuid());
		
		store.addLink(t, newMessage, Relation.Message.hasTopic.get(), m.getTopicGUID());
		store.addLink(t, m.getTopicGUID(), Relation.Topic.hasMessage.get(), newMessage);
		
		store.commit(t);
		
		m.setGuid(newMessage);
		return newMessage;
		
	}
	
	public GUID getUserGUIDFromUsername(String username) {
		GUID userGUID = null;
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable user = new Variable("user");
		
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, user, Relation.User.hasUsername.get(), username);
		
		q.where.add(w1);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		if(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				userGUID = (GUID) result.get(user);
			}catch(Exception e) {
				System.out.println("Failed to convert message details to message");
				e.printStackTrace();
			}
		}
		
		return userGUID;
		
	}
	
	/**
	 * Crate a new Topic
	 * @param topicName
	 * @param attachment
	 * @return
	 */
	public GUID newTopic(GUID groupId, String topicName, String attachment) {
		GUID t = store.begin();
		
		GUID newTopic = new GUID();
		
		store.addLink(groupId, Relation.Group.hasTopic.get(), newTopic);
		store.addLink(newTopic, Relation.Topic.hasGroup.get(), groupId);
		
		store.addLink(newTopic, Relation.Topic.hasName.get(), topicName);
		store.addLink(newTopic, Relation.Topic.hasAttachment.get(), attachment);
		
		store.commit();
		return newTopic;
	}

	/**
	 * Create a new user
	 * @param username
	 * @param name
	 * @return
	 */
	public GUID newUser(String username, String name) {
		GUID t = store.begin();
		
		GUID newUser = new GUID();
		
		store.addLink(t, newUser, Relation.User.hasName.get(), name);
		store.addLink(t, newUser, Relation.User.hasUsername.get(), username);
		
		store.commit(t);
		
		return newUser;
	}

	/**
	 * Create a new Group
	 * @param name
	 * @return
	 */
	public GUID newGroup(String name) {
		GUID t = store.begin();
		GUID newGroup = new GUID();
		store.addLink(newGroup, Relation.Group.hasName.get(), name);
		store.commit();
		return newGroup;
	}

	/**
	 * Load topic data
	 * @param topicID
	 * @return
	 */
	public Topic loadTopic(GUID topicID) {
		Topic topic = new Topic();
		topic.setGuid(topicID);
		
		Iterator<Object> group = store.get(topicID, Relation.Topic.hasGroup.get());
		Iterator<Object> name = store.get(topicID, Relation.Topic.hasName.get());
		Iterator<Object> attachment = store.get(topicID, Relation.Topic.hasAttachment.get());
		
		if(name.hasNext()) {
			topic.setName((String) name.next()); 
		}
		
		if(attachment.hasNext()) {
			topic.setAttachment((String) attachment.next()); 
		}
		
		if(group.hasNext()) {
			topic.setGroupGuid((GUID) group.next()); 
		}
		
		return topic;
	}

	/**
	 * Load user data
	 * @param userID
	 * @return
	 */
	public User loadUser(GUID userID) {
		User u = new User();
		u.setGuid(userID);
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable username = new Variable("username");
		Variable name = new Variable("name");
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, userID, Relation.User.hasName.get(), name);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, userID, Relation.User.hasUsername.get(), username);
		
		q.where.add(w1);
		q.where.add(w2);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		if(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				u.setName((String) result.get(name));
				u.setUsername((String) result.get(username));
			}catch(Exception e) {
				System.out.println("Failed to convert user details to user object");
				e.printStackTrace();
			}
		}
		return u;
	}
	
	public List<Topic> getGroupTopicsForMemeber(GUID group, GUID user){
		List<Topic> topicsForMember = new ArrayList<Topic>();
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable topic = new Variable("topic");
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, user, Relation.User.subscribedTo.get(), topic);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, topic, Relation.Topic.hasGroup.get(), group);
		
		q.where.add(w1);
		q.where.add(w2);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				Topic topicObj = Topic.fromGUID((GUID) result.get(topic));
				topicsForMember.add(topicObj);
			}catch(Exception e) {
				System.out.println("Failed to convert user details to user object");
				e.printStackTrace();
			}
		}
		
		return topicsForMember;
	}
	
	/**
	 * Load group data
	 * @param groupID
	 * @return
	 */
	public Group loadGroup(GUID groupID) {
		Group g = new Group();
		g.setGuid(groupID);
		
		Iterator<Object> name = store.get(groupID, Relation.Group.hasName.get());
		
		if(name.hasNext()) {
			g.setName((String) name.next()); 
		}
		
		return g;
	}

	/**
	 * Generate list of all topics
	 * @return
	 */
	public List<Topic> allTopics(){
		List<Topic> topics = new ArrayList<Topic>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable topic = new Variable("topic");
		Variable topicName = new Variable("topicName");
		Variable topicAttachment = new Variable("topicAttachment");
		Variable group = new Variable("group");
		
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, topic, Relation.Topic.hasName.get(), topicName);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, topic, Relation.Topic.hasAttachment.get(), topicAttachment);
		PDChange<GUID, Object, GUID> w3 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, topic, Relation.Topic.hasGroup.get(), group);
		
		q.where.add(w1);
		q.where.add(w2);
		q.where.add(w3);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				Topic topicObject = new Topic();
				topicObject.setGuid((GUID) result.get(topic)); 
				topicObject.setName((String) result.get(topicName));
				topicObject.setAttachment((String) result.get(topicAttachment));
				topicObject.setGroupGuid((GUID) result.get(group));
				topics.add(topicObject);
			}catch(Exception e) {
				System.out.println("Failed to convert message details to message");
				e.printStackTrace();
			}
		}
		
		return topics;
	}
	
	public List<Status> allStatus(){
		List<Status> statusList = new ArrayList<Status>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable status = new Variable("status");
		Variable statusText = new Variable("statusText");
		
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, status, Relation.Status.hasText.get(), statusText);
		
		q.where.add(w1);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				statusList.add(Status.fromGUID((GUID) result.get(status)));
			}catch(Exception e) {
				System.out.println("Failed to convert message details to message");
				e.printStackTrace();
			}
		}
		
		return statusList;
	}
	
	public List<Status> allStatusForUser(GUID u){
		List<Status> statusList = new ArrayList<Status>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable status = new Variable("status");
		
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, u, Relation.User.hasStatus.get(), status);
		
		q.where.add(w1);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				statusList.add(Status.fromGUID((GUID) result.get(status)));
			}catch(Exception e) {
				System.out.println("Failed to convert message details to message");
				e.printStackTrace();
			}
		}
		
		return statusList;
	}

	/**
	 * Generate List of all groups
	 * @return
	 */
	public List<Group> allGroups(){
		List<Group> groups = new ArrayList<Group>();
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable group = new Variable("group");
		Variable groupName = new Variable("groupName");
		
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, group, Relation.Group.hasName.get(), groupName);
		
		q.where.add(w1);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				Group groupObject = new Group();
				groupObject.setGuid((GUID) result.get(group)); 
				groupObject.setName((String) result.get(groupName));
				groups.add(groupObject);
			}catch(Exception e) {
				System.out.println("Failed to convert group details to group object");
				e.printStackTrace();
			}
		}
		
		return groups;
		
	}

	/**
	 * Generate list of all users
	 * @return
	 */
	public List<User> allUsers(){
		List<User> users = new ArrayList<User>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable user = new Variable("user");
		Variable username = new Variable("username");
		Variable name = new Variable("name");
		
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, user, Relation.User.hasName.get(), name);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, user, Relation.User.hasUsername.get(), username);
		
		q.where.add(w1);
		q.where.add(w2);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				User userObject = new User();
				userObject.setGuid((GUID) result.get(user)); 
				userObject.setName((String) result.get(name));
				userObject.setUsername((String) result.get(username));
				users.add(userObject);
			}catch(Exception e) {
				System.out.println("Failed to convert user details to user object");
				e.printStackTrace();
			}
		}
		
		return users;
	}

	/**
	 * Generate list of members associated with topic
	 * @param topic
	 * @return
	 */
	public List<User> getTopicMembers(GUID topic){
		List<User> members = new ArrayList<User>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable user = new Variable("user");
		Variable name = new Variable("name");
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, topic, Relation.Topic.hasSubscribee.get(), user);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, user, Relation.User.hasName.get(), name);
		
		q.where.add(w1);
		q.where.add(w2);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				User userObject = new User();
				userObject.setGuid((GUID) result.get(user)); 
				userObject.setName((String) result.get(name));
				members.add(userObject);
			}catch(Exception e) {
				System.out.println("Failed to convert user details to user object");
				e.printStackTrace();
			}
		}
		
		return members;
	}
	
	public List<Topic> getTopicsForMemeber(GUID user){
		List<Topic> userTopics = new ArrayList<Topic>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable topic = new Variable("topic");
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, user, Relation.User.subscribedTo.get(), topic);
		
		q.where.add(w1);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				Topic topicObj = Topic.fromGUID((GUID) result.get(topic));
				userTopics.add(topicObj);
			}catch(Exception e) {
				System.out.println("Failed to convert user details to user object");
				e.printStackTrace();
			}
		}
		return userTopics;
	}

	/**
	 * Get all messages in topic
	 * @param topic
	 * @return
	 */
	public List<Message> getTopicMessages(GUID topic){
		List<Message> messages = new ArrayList<Message>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable message = new Variable("message");
		Variable messageContent = new Variable("messageContent");
		Variable messageSender = new Variable("messageSender");
		Variable messageSenderName = new Variable("messageSenderName");
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, topic, Relation.Topic.hasMessage.get(), message);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, message, Relation.Message.hasContent.get(), messageContent);
		PDChange<GUID, Object, GUID> w3 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, message, Relation.Message.hasSender.get(), messageSender);
		PDChange<GUID, Object, GUID> w4 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, messageSender, Relation.User.hasName.get(), messageSenderName);
		
		q.where.add(w1);
		q.where.add(w2);
		q.where.add(w3);
		q.where.add(w4);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				Message messageObj = new Message();
				messageObj.setGuid((GUID) result.get(message));
				messageObj.setContent((String) result.get(messageContent));
				messageObj.setSender((GUID) result.get(messageSender));
				messageObj.setSenderUser(User.fromGUID(messageObj.getSender()));
				messages.add(messageObj);
			}catch(Exception e) {
				System.out.println("Failed to convert message details to message object");
				e.printStackTrace();
			}
		}
		
		return messages;
	}

	/**
	 * Subscribe user to topic
	 * @param user
	 * @param topic
	 * @return success
	 */
	public boolean subscribeToTopic(GUID user, GUID topic) {
		GUID t = store.begin();
		store.addLink(user, Relation.User.subscribedTo.get(), topic);
		store.addLink(topic, Relation.Topic.hasSubscribee.get(), user);
		store.commit();
		return true; //TODO, check if successful
	}
	
	/**
	 * unsubscribe user from topic
	 * @param user
	 * @param topic
	 * @return success
	 */
	public boolean unsubscribeFromTopic(GUID user, GUID topic) {
		GUID t = store.begin();
		store.removeLink(t, user, Relation.User.subscribedTo.get(), topic);
		store.removeLink(t, topic, Relation.Topic.hasSubscribee.get(), user);
		store.commit(t);
		return true; //TODO, check if successful
	}
	
	/**
	 * Add user to group
	 * @param user
	 * @param group
	 * @return success
	 */
	public boolean joinGroup(GUID user, GUID group) {
		GUID t = store.begin();
		store.addLink(t, user, Relation.User.memberOf.get(), group);
		store.addLink(t, group, Relation.Group.hasMemeber.get(), user);
		store.commit(t);
		return true; //TODO, check if task was successful or not.
	}
	
	/**
	 * Remove user from group
	 * @param user
	 * @param group
	 * @return success
	 */
	public boolean leaveGroup(GUID user, GUID group) {
		GUID t = store.begin();
		store.removeLink(t, user, Relation.User.memberOf.get(), group);
		store.removeLink(t, group, Relation.Group.hasMemeber.get(), user);
		store.commit(t);
		return true; //TODO, make this actually check if successful or not.
	}
	
	/**
	 * Return all the groups that the user is a member of.
	 * @param user
	 * @return
	 */
	public List<Group> getGroupsForMemeber(GUID user){
		List<Group> groups = new ArrayList<Group>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable group = new Variable("group");
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, user, Relation.User.memberOf.get(), group);
		
		q.where.add(w1);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator  = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				GUID groupGuid = (GUID) result.get(group);
				Group g = Group.fromGUID(groupGuid);
				groups.add(g);
				
			}catch(Exception e) {
				System.out.println("Failed to convert group details to group object");
				e.printStackTrace();
			}
		}
		
		
		return groups;
	}

	/**
	 * Return all the topics for the group
	 * @param group
	 * @return
	 */
	public List<Topic> getTopicsForGroup(GUID group){
		List<Topic> topics = new ArrayList<Topic>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable topic = new Variable("topic");
		Variable topicName = new Variable("topicName");
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, group, Relation.Group.hasTopic.get(), topic);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, topic, Relation.Topic.hasName.get(), topicName);
		
		q.where.add(w1);
		q.where.add(w2);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				Topic topicObj = new Topic();
				topicObj.setGuid((GUID) result.get(topic)); 
				topicObj.setName((String) result.get(topicName));
				topics.add(topicObj);
			}catch(Exception e) {
				System.out.println("Failed to convert topic details to topic object");
				e.printStackTrace();
			}
		}
		
		return topics;
	}
	
	/**
	 * Return all the users for the group
	 * @param group
	 * @return
	 */
	public List<User> getMembersForGroup(GUID group){
		List<User> members = new ArrayList<User>();
		
		GUID t = store.begin();
		Query q = new Query(
			new ArrayList<Variable>(), 
			new ArrayList<PDChange<GUID, Object, GUID>>(),
			store
		);
		
		Variable user = new Variable("user");
		Variable name = new Variable("name");
		
		PDChange<GUID, Object, GUID> w1 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, group, Relation.Group.hasMemeber.get(), user);
		PDChange<GUID, Object, GUID> w2 = new PDChange<GUID, Object, GUID>(ChangeType.LINK_EXISTS, t, user, Relation.User.hasName.get(), name);
		
		q.where.add(w1);
		q.where.add(w2);
		
		java.util.Iterator<Assignment<GUID, Object, GUID>> assignmentIterator = q.execute(null);
		while(assignmentIterator.hasNext()) {
			Assignment<GUID, Object, GUID> result = assignmentIterator.next();
			try {
				User userObject = new User();
				userObject.setGuid((GUID) result.get(user)); 
				userObject.setName((String) result.get(name));
				members.add(userObject);
			}catch(Exception e) {
				System.out.println("Failed to convert user details to user object");
				e.printStackTrace();
			}
		}
		
		return members;
	}
}
