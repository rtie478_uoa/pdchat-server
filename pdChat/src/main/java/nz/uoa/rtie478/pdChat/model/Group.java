package nz.uoa.rtie478.pdChat.model;

import java.util.List;

import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import pdstore.GUID;

public class Group {
	private String id;
	private transient GUID guid;
	private String name;
	private long creation_timestamp;
	
	private List<Topic> topics;
	private List<User> members;
	
	public void populateTopics() {
		this.setTopics(ChatStore.getInstance().getTopicsForGroup(this.guid));
	}
	
	public void populateMembers() {
		this.setMembers(ChatStore.getInstance().getMembersForGroup(this.guid));
	}
	
	public void populateTopicsForUser(GUID user) {
		this.setTopics(ChatStore.getInstance().getGroupTopicsForMemeber(this.guid, user));
	}
	
	
	public String getId() {
		return id;
	}
	public GUID getGuid() {
		return guid;
	}
	public void setGuid(GUID guid) {
		this.guid = guid;
		this.id = GUID.toHexString(this.guid.toByteArray());
		this.creation_timestamp = this.guid.time();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getCreationTimestamp() {
		return creation_timestamp;
	}
	public List<Topic> getTopics() {
		return topics;
	}
	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}
	public List<User> getMembers() {
		return members;
	}
	public void setMembers(List<User> members) {
		this.members = members;
	}
	
	
	public static Group fromGUID(GUID guid, boolean loadDetails) {
		Group g;
		if(loadDetails) {
			g = ChatStore.getInstance().loadGroup(guid);
		}else {
			g = new Group();
			g.setGuid(guid);
		}
		
		
		return g;
	}
	public static Group fromGUID(GUID guid) {
		return Group.fromGUID(guid, true);
	}
	public static Group fromHex(String hex, boolean loadDetails) {
		GUID guid = new GUID(hex);
		return Group.fromGUID(guid, loadDetails);
	}
	public static Group fromHex(String hex) {
		return Group.fromHex(hex, true);
	}
	
}
