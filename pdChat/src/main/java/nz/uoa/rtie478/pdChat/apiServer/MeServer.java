package nz.uoa.rtie478.pdChat.apiServer;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import nz.uoa.rtie478.pdChat.model.Group;
import nz.uoa.rtie478.pdChat.model.Status;
import nz.uoa.rtie478.pdChat.model.Topic;
import nz.uoa.rtie478.pdChat.model.User;
import nz.uoa.rtie478.pdChat.pdstore.ChatStore;

@Path("me")
public class MeServer {
	Gson gson = new Gson();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadUserMe(@Context ContainerRequestContext context) {
		try {
			User authenticatedUser = AuthFilter.getUser(context);
			authenticatedUser.loadStatus();
			authenticatedUser.getGroups();
			authenticatedUser.getGroups().forEach(group -> {
				group.populateTopicsForUser(authenticatedUser.getGuid());
			});
			return Response.ok(gson.toJson(authenticatedUser)).build();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@GET
	@Path("/groups")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadMyGroups(@Context ContainerRequestContext context) {
		User authenticatedUser = AuthFilter.getUser(context);
		List<Group> groups = ChatStore.getInstance().getGroupsForMemeber(authenticatedUser.getGuid());
		groups.forEach(group -> {
			group.populateTopicsForUser(authenticatedUser.getGuid());
		});
		return Response.ok(gson.toJson(groups)).build();
	}
	
	@GET
	@Path("/topics")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadMyTopics(@Context ContainerRequestContext context) {
		User authenticatedUser = AuthFilter.getUser(context);
		List<Topic> topics = ChatStore.getInstance().getTopicsForMemeber(authenticatedUser.getGuid());
		return Response.ok(gson.toJson(topics)).build();
	}
	
	@GET
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadUserBasic(@Context ContainerRequestContext context) {
		User authenticatedUser = AuthFilter.getUser(context);
		return Response.ok(gson.toJson(authenticatedUser)).build();
	}
	
	@GET
	@Path("/status")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadUserStatus(@Context ContainerRequestContext context) {
		User authenticatedUser = AuthFilter.getUser(context);
		Status s = authenticatedUser.getStatus();
		return Response.ok(gson.toJson(s)).build();
	}
	
	
	
	
}
