package nz.uoa.rtie478.pdChat.apiServer;

import java.lang.reflect.Method;
import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.internal.util.Base64;

import com.google.gson.Gson;

import nz.uoa.rtie478.pdChat.model.User;
 
/**
 * This filter verify the access permissions for a user
 * based on username and passowrd provided in request
 * */
@Provider
public class AuthFilter implements javax.ws.rs.container.ContainerRequestFilter
{
     
    @Context
    private ResourceInfo resourceInfo;
    
    static Gson gson = new Gson();
     
    public static final String AUTHORIZATION_PROPERTY = "Authorization";
    public static final String AUTHENTICATION_SCHEME = "Basic";
    
    public static Response generateAcccessDeniedResponse() {
    	return  Response.status(Response.Status.UNAUTHORIZED).entity(gson.toJson("You cannot access this resource")).build();
    }
    
    public static Response generateAccessForbiddenResponse() {
    	return Response.status(Response.Status.FORBIDDEN).entity(gson.toJson("Access blocked for all users !!")).build();
    }
    
    @Override
    public void filter(ContainerRequestContext requestContext)
    {
        Method method = resourceInfo.getResourceMethod();
        //Access allowed for all
        if( ! method.isAnnotationPresent(PermitAll.class))
        {
            //Access denied for all
            if(method.isAnnotationPresent(DenyAll.class))
            {
                requestContext.abortWith(AuthFilter.generateAccessForbiddenResponse());
                return;
            }
              
            //Get request headers
            final MultivaluedMap<String, String> headers = requestContext.getHeaders();
              
            //Fetch authorization header
            final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);
              
            //If no authorization information present; block access
            if(authorization == null || authorization.isEmpty())
            {
                requestContext.abortWith(AuthFilter.generateAcccessDeniedResponse());
                return;
            }
              
            //Get encoded username and password
            final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");
              
            //Decode username and password
            String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));;
  
            //Split username and password tokens
            final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken();
              
            if( ! isUserAllowed(username, password))
            {
                requestContext.abortWith(AuthFilter.generateAcccessDeniedResponse());
                return;
            }
        }
    }
    private boolean isUserAllowed(final String username, final String password)
    {
        User u = User.fromUsername(username);
        if(u == null) {
        	return false;
        }
        
        return u.checkPassword(password);
    }
    
    public static User getUser(ContainerRequestContext context) {
    	String authString = context.getHeaderString(AuthFilter.AUTHORIZATION_PROPERTY);
		
    	if(authString == null) {
			context.abortWith(AuthFilter.generateAcccessDeniedResponse());
		}
		
		//Get encoded username and password
        final String encodedUserPassword = authString.replaceFirst(AuthFilter.AUTHENTICATION_SCHEME + " ", "");
          
        //Decode username and password
        String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));;

        //Split username and password tokens
        final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
        final String username = tokenizer.nextToken();
        final String password = tokenizer.nextToken();
        
        User u = User.fromUsername(username);
        
        if(u == null) {
			context.abortWith(AuthFilter.generateAcccessDeniedResponse());
		}
        
        if(u.checkPassword(password)) {
        	return u;
        }
        else {
    		context.abortWith(AuthFilter.generateAcccessDeniedResponse());
        }
        return null;
    }
    
    public static boolean checkRequestForSelf(ContainerRequestContext context, String uid) {
    	User authenticatedUser = AuthFilter.getUser(context);
    	if(authenticatedUser == null) return false;
    	return authenticatedUser.getId().equals(uid);
    }
}

