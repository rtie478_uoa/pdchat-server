package nz.uoa.rtie478.pdChat.model;

import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import pdstore.GUID;

/**
 * Object to represent a users current status.
 * @author rtie478
 */
public class Status {
	//Status variables
	private transient GUID guid;
	private String id;
	private long timestamp;
	private String status; //Status is the highest level status -> eg, Online, Working, Offline, etc.
	private String mood; //Mood is a custom text field that users can set to explain what they are doing if they want to.
	
	//House keeping variables.
	private String userid;
	private User user; //The user that this status is for.
	
	private String topicid;
	private Topic topic; //The topic that the user has associated their status with.
	
	private String groupid;
	private Group group; //The group the user has associated their status with.
	
	public void loadGroup() {
		this.group = Group.fromHex(this.groupid);
	}
	public void setGroup(Group g) {
		this.group = g;
	}
	public void setGroup(GUID g) {
		this.group = Group.fromGUID(g);
	}
	public Group getGroup() {
		if(this.group != null) {
			return this.group;
		}
		else if(this.groupid != null) {
			this.group = Group.fromHex(this.groupid);
			return this.group;
		}
		else {
			return null;
		}
	}
	
	public void loadTopic() {
		this.topic = Topic.fromHex(this.topicid);
	}
	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	public void setTopic(GUID topic) {
		this.topic = Topic.fromGUID(topic);
	}
	public Topic getTopic() {
		if(this.topic != null) {
			return this.topic;
		}
		else if(this.topicid != null) {
			this.topic = Topic.fromHex(this.topicid);
			return this.topic;
		}
		else {
			return null;
		}
	}
	
	public void loadUser() {
		this.user = User.fromHex(this.userid);
	}
	public void setUser(User user) {
		this.user = user;
	}
	public void setUser(GUID user) {
		this.user = User.fromGUID(user);
	}
	public User getUser() {
		if(this.user != null) {
			return this.user;
		}
		else if(this.userid != null) {
			this.user = User.fromHex(this.userid);
			return this.user;
		}
		else {
			return null;
		}
	}
	
	
	public void setGuid(GUID status) {
		this.guid = status;
		this.id = GUID.toHexString(this.guid.toByteArray());
		this.timestamp = this.guid.time();
	}

	public GUID getGuid() {
		return this.guid;
	}
	
	public String getId() {
		return this.id;
	}
	


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMood() {
		return mood;
	}

	public void setMood(String mood) {
		this.mood = mood;
	}

	public static Status fromGUID(GUID guid, boolean loadDetails) {
		Status t;
		if(loadDetails) {
			t = ChatStore.getInstance().loadStatus(guid);
		}else {
			t = new Status();
			t.setGuid(guid);
		}
		
		
		return t;
	}
	
	public static Status fromGUID(GUID guid) {
		return Status.fromGUID(guid, true);
	}
	public static Status fromHex(String hex, boolean loadDetails) {
		GUID guid = new GUID(hex);
		return Status.fromGUID(guid, loadDetails);
	}
	public static Status fromHex(String hex) {
		return Status.fromHex(hex, true);
	}
	
}
