package nz.uoa.rtie478.pdChat.apiServer;

import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.internal.util.Base64;
import org.glassfish.tyrus.core.RequestContext;

import com.google.gson.Gson;

import nz.uoa.rtie478.pdChat.model.Status;
import nz.uoa.rtie478.pdChat.model.Topic;
import nz.uoa.rtie478.pdChat.model.User;
import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import pdstore.GUID;

@Path("user")
public class UserServer {
	Gson gson = new Gson();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getUsers() {
		List<User> users = ChatStore.getInstance().allUsers();
		return gson.toJson(users);
	}
	
	
	
	@GET
	@Path("/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadUser(@PathParam(value = "userid") String userid, @Context ContainerRequestContext context) {
		try {
			User authenticatedUser = AuthFilter.getUser(context);
			User user = User.fromHex(userid);
			
			if(!authenticatedUser.isAdmin() && !authenticatedUser.getId().equals(userid)) {
				return AuthFilter.generateAcccessDeniedResponse();
			}
			
			user.loadStatus();
			user.getGroups();
			user.getGroups().forEach(group -> {
				group.populateTopicsForUser(user.getGuid());
			});
			return Response.ok(gson.toJson(user)).build();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@GET
	@Path("/me")
	@Produces(MediaType.APPLICATION_JSON)
	public String loadUserMe(@Context ContainerRequestContext context) {
		try {
			User authenticatedUser = AuthFilter.getUser(context);
			authenticatedUser.loadStatus();
			authenticatedUser.getGroups();
			authenticatedUser.getGroups().forEach(group -> {
				group.populateTopicsForUser(authenticatedUser.getGuid());
			});
			return gson.toJson(authenticatedUser);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@GET
	@Path("/byusername/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public String loadUserByUsername(@PathParam(value = "username") String username) {
		try {
			User user = User.fromUsername(username);
			if(user == null) return null;
			user.loadStatus();
			user.getGroups();
			user.getGroups().forEach(group -> {
				group.populateTopicsForUser(user.getGuid());
			});
			return gson.toJson(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@GET
	@Path("/{userid}/topics")
	@Produces(MediaType.APPLICATION_JSON)
	public String loadUserTopics(@PathParam(value = "userid") String userid) {
		try {
			GUID user = new GUID(userid);
			List<Topic> userTopics = ChatStore.getInstance().getTopicsForMemeber(user);
			userTopics.forEach((Topic topic) -> {
				topic.getGroup();
			});
			return gson.toJson(userTopics);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@PermitAll()
	public String newUser(String json) {
		User inputUser = gson.fromJson(json, User.class);
		GUID newUserId = ChatStore.getInstance().newUser(inputUser.getUsername(), inputUser.getName());
		User newUser = ChatStore.getInstance().loadUser(newUserId);
		return gson.toJson(newUser);
	}
	
	@POST
	@Path("/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String editUser(@PathParam(value = "userid") String userid, String json) {
		return gson.toJson(false);
	}
	
	@PUT
	@Path("/{userid}/setstatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String setUserStatus(@PathParam(value = "userid") String userid, String json) {
		GUID user = new GUID(userid);
		Status s = gson.fromJson(json, Status.class);
		boolean success = ChatStore.getInstance().setUserStatus(user, s);
		return gson.toJson(success);
	}


}
