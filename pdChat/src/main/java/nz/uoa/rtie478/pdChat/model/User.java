package nz.uoa.rtie478.pdChat.model;

import java.util.List;

import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import pdstore.GUID;

public class User {
	private String id;
	private transient GUID guid;
	private String name;
	private String username;
	private long creation_timestamp;
	
	//When a authenticated user loads this user as a topic member, this will be set to the members topic subscription state.
	private boolean isTopicMemeber = false;
	
	public void setSubscribed(boolean state) {
		this.isTopicMemeber = state;
	}
	
	public boolean getSubscribed() {
		return this.isTopicMemeber;
	}
	
	private Status status;
	private List<Group> groups;
	
	/**
	 * Loads the groups and returns the list.
	 * Can be used to just load the group by ignoring the returned value.
	 */
	public List<Group> getGroups() {
		if(this.groups == null) {
			this.groups = ChatStore.getInstance().getGroupsForMemeber(this.guid);
		}
		return this.groups;
	}
	
	public boolean isAdmin() {
		return this.username.equals("admin"); //Good enough for a prototype system to test this idea.
	}
	
	public void loadStatus() {
		Status s = ChatStore.getInstance().getUserStatus(this.guid);
		if(s != null) {
			this.status = s;
			s.getGroup();
			s.getTopic();
		}
	}
	
	public Status getStatus() {
		this.loadStatus();
		return this.status;
	}
	
	public void setGuid(GUID guid) {
		this.guid = guid;
		this.id = GUID.toHexString(this.guid.toByteArray());
		this.creation_timestamp = this.guid.time();
	}
	
	public long getCreationTimestamp() {
		return this.creation_timestamp;
	}
	
	public String getId() {
		return id;
	}
	public GUID getGuid() {
		return guid;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public boolean checkPassword(String password) {
		return true;
	}
	
	public static User fromUsername(String username) {
		GUID user = ChatStore.getInstance().getUserGUIDFromUsername(username);
		if(user != null) {
			return User.fromGUID(user);
		}
		return null;
	}
	
	public static User fromGUID(GUID userGUID, boolean loadDetails) {
		User u;
		if(loadDetails) {
			u = ChatStore.getInstance().loadUser(userGUID);
		}else {
			u = new User();
			u.setGuid(userGUID);
		}
		
		
		return u;
	}
	public static User fromGUID(GUID userGUID) {
		return User.fromGUID(userGUID, true);
	}
	public static User fromHex(String hex, boolean loadDetails) {
		GUID userGUID = new GUID(hex);
		return User.fromGUID(userGUID, loadDetails);
	}
	public static User fromHex(String hex) {
		return User.fromHex(hex, true);
	}

}
