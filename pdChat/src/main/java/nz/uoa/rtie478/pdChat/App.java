package nz.uoa.rtie478.pdChat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

import javax.websocket.DeploymentException;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.tyrus.server.Server;

import com.google.gson.Gson;

import nz.uoa.rtie478.pdChat.apiServer.AuthFilter;
import nz.uoa.rtie478.pdChat.apiServer.GroupServer;
import nz.uoa.rtie478.pdChat.apiServer.MeServer;
import nz.uoa.rtie478.pdChat.apiServer.MessageServer;
import nz.uoa.rtie478.pdChat.apiServer.StatusServer;
import nz.uoa.rtie478.pdChat.apiServer.TopicServer;
import nz.uoa.rtie478.pdChat.apiServer.UserServer;
import nz.uoa.rtie478.pdChat.model.Message;
import nz.uoa.rtie478.pdChat.model.User;
import nz.uoa.rtie478.pdChat.pdstore.ChatStore;
import nz.uoa.rtie478.pdChat.pdstore.Connection;
import nz.uoa.rtie478.pdChat.websocket.WebsocketServer;
import pdstore.GUID;


public class App 
{
	Server socketServer;
	HttpServer server;
	Gson gson = new Gson();
	
    public static void main(String[] args) {
    	System.out.println("=== PD Chat Server ===");
    	App a = new App();
    		
    		
//    		GUID group = ChatStore.getInstance().newGroup("test Group");
//        	GUID user = ChatStore.getInstance().newUser("subserver", "Ryan Tiedemann");
//    		GUID topic = ChatStore.getInstance().newTopic(group, "test Topic", "http://pdchat.xyz");
//    		
//    		Message m = new Message();
//    		m.setSender(user);
//    		m.setTopic(topic);
//    		m.setContent("Hello World!");
//    		m.insertSelf();
//    		
//    		System.out.println(gson.toJson(m));
    		
    		
//    		Message m1 = Message.fromHex("0000e215b6b555a500358e72ae399512", true);
//    		System.out.println(gson.toJson(m1));
//    		
//    		Topic t = Topic.fromHex("0000e215b6b555a500358e72ae301f32", true);
//    		System.out.println(gson.toJson(t));
//    		
//    		User u = User.fromHex("0000e215b6b555a500358e72ae2ff822", true);
//    		System.out.println(gson.toJson(u));
//    		
//    		System.out.println(new GUID());
//    		System.out.println(new GUID());
    		
//    		ChatStore.getInstance().removeAllUserStatus(User.fromHex("000094de80bf48640035905a9cdab4e0", false));
    		
    		
    }
    
    public App() {
    	//Start the server by default.
    	this.runServer();
    	
    	//Command loop
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
        	System.out.print("Enter a command: ");
        	try {
    			String command = reader.readLine();
    			handleInput(command);
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        }
    }
    
    public void handleInput(String command) {
    	switch(command) {
    		case "exit":
				this.stopApiServer();
				this.stopSocketServer();
				System.exit(0);
				break;
				
    		case "stop":
    			this.stopApiServer();
    			this.stopSocketServer();
    			break;
    			
    		case "start":
    			this.startApiServer();
    			this.startSocketServer();
    			break;
    			
    		case "help":
    			showHelp();
    			break;
    		case "?":
    			showHelp();
    			break;
    			
    		case "start-api":
    			this.startApiServer();
    			break;
    			
    		case "stop-api":
    			this.stopApiServer();
    			break;
    			
    		case "start-websocket":
    			this.startSocketServer();
    			break;
    			
    		case "stop-websocket":
    			this.stopSocketServer();
    			break;
    			
    		case "show-socket-clients":
    			WebsocketServer.showConnectedUsers();
    			break;
    			
    		default:
    			System.out.println("Unknown command");
    			break;
    	}
    }
    
    public void showHelp() {
    	System.out.println("Commands: ");
    	System.out.println("---------- Common ----------");
		System.out.println("help - Shows this help information");
		System.out.println("start - Starts the HTTP & Websocket servers");
		System.out.println("stop - Stops the HTTP & Websocket servers");
		System.out.println("exit - Stops the HTTP & Websocket servers + exits program");
		
		System.out.println("---------- API Server ----------");
		System.out.println("start-api - Start the HTTP server");
		System.out.println("stop-api - Stop the HTTP server");

		System.out.println("---------- Websocket Server ----------");
		System.out.println("start-websocket - Start the websocket server");
		System.out.println("stop-websocket - Stop the websocket server");
		System.out.println("\n");
    }
 
    public void runServer() {
    		
		this.setupApiServer();
		this.setupWebSocket();
		
		this.startApiServer();
		this.startSocketServer();
    		
    }
    
    public void stopApiServer() {
    	if(this.server != null) this.server.stop();
    }
    
    public void stopSocketServer() {
    	if(this.socketServer != null) this.socketServer.stop();
    }
    
    public void startApiServer() {
    	if(this.server == null) this.setupApiServer();
    	//Start API server
		try {
			server.start();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    }
    
    public void startSocketServer() {
    	if(this.socketServer == null) this.setupWebSocket();
    	//Start websocket server
		try {
    		this.socketServer.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void setupApiServer() {
    	URI baseUri = UriBuilder.fromUri("http://192.168.178.80/").port(8040).build();
		ResourceConfig resourceConfig = new ResourceConfig();
		
		
		//Resources
		resourceConfig.register(UserServer.class);
		resourceConfig.register(GroupServer.class);
		resourceConfig.register(MessageServer.class);
		resourceConfig.register(TopicServer.class);
		resourceConfig.register(StatusServer.class);
		resourceConfig.register(MeServer.class);
		
		//Auth Filter
		resourceConfig.register(AuthFilter.class);
		
		
		server = GrizzlyHttpServerFactory.createHttpServer(baseUri, resourceConfig);
    }
    
    public void setupWebSocket() {
    	//Socket Server stuff.
    	this.socketServer = new Server("192.168.178.80", 8028, "/test", WebsocketServer.class);
    }
}
