package nz.uoa.rtie478.pdChat.websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import com.google.gson.Gson;

import nz.uoa.rtie478.pdChat.model.Message;
import nz.uoa.rtie478.pdChat.model.Topic;
import nz.uoa.rtie478.pdChat.model.User;
import nz.uoa.rtie478.pdChat.pdstore.ChatStore;

@ServerEndpoint( 
		  value="/chat/{userid}", 
		  decoders = MessageDecoder.class, 
		  encoders = MessageEncoder.class )
public class WebsocketServer {
	
	enum MessageTypes{
		SHOW,
		Chat
	}
	
	public static List<String> getGuestTopicViewers(String topicId) {
		List<String> topicGuestSessions = WebsocketServer.currentlyOpenTopics.get(topicId);
		ArrayList<String> topicGuestUids = new ArrayList<String>();
		if(topicGuestSessions == null) {
			return new ArrayList<String>();
		}else {
			//Convert session IDS to the actual user ids
			topicGuestSessions.forEach(sessionid -> {
				SocketUser socketUser = WebsocketServer.connectedUsers.get(sessionid);
				if(socketUser != null) {
					User u = socketUser.getUser();	
					topicGuestUids.add(u.getId());
				}
			});
			return topicGuestUids;
		}
	}
	
	public static List<User> getGuestTopicViewerUsers(String topicId){
		ArrayList<User> guestUsers = new ArrayList<User>();
		List<String> guestViewers = WebsocketServer.getGuestTopicViewers(topicId);
		guestViewers.forEach(uid -> {
			User guestUser = User.fromHex(uid);
			guestUsers.add(guestUser);
		});
		return guestUsers;
	}
  
    private Session session;
    
    //All connected endpoints.
    private static Set<WebsocketServer> connectedSessions = new CopyOnWriteArraySet<WebsocketServer>();
    
    //Maintain collection of connected users.
    private static HashMap<String, SocketUser> connectedUsers = new HashMap<String, SocketUser>();
    private static HashMap<String, String> connectedUserIdSessionIdMap = new HashMap<String, String>();
    
    //Map topicid -> session id for guest sessions.
    private static HashMap<String, ArrayList<String>> currentlyOpenTopics = new HashMap<String, ArrayList<String>>();
    
    private static ChatStore chatStore = ChatStore.getInstance();
    Gson gson = new Gson();
    
    public static void userLoadedTopic(User u, Topic t) {
    	String userid = u.getId();
    	String sessionid = connectedUserIdSessionIdMap.get(userid);
    	if(sessionid != null) {
    		//Get the socketUser instance.
    		SocketUser su = connectedUsers.get(sessionid);
    		
    		//Remove old mapping
    		if(su.getCurrentTopic() != null && su.getCurrentTopic().getId() != null && currentlyOpenTopics.get(su.getCurrentTopic().getId()) != null) {
    			currentlyOpenTopics.get(su.getCurrentTopic().getId()).remove(sessionid);
    		}
    		
    		//Insert new mappings
    		su.setCurrentTopic(t);
    		
    		//If the topic is not already in the currently opened list, then add it as a new list.
    		if(currentlyOpenTopics.get(t.getId()) == null) {
    			currentlyOpenTopics.put(t.getId(), new ArrayList<String>());
    		}
    		
    		//Add the new currently open topic mapping.
    		currentlyOpenTopics.get(t.getId()).add(sessionid);
    		
    		//Print the topic open to the console.
    		System.out.println(su.getUser().getName() + " opened topic " + t.getName());
    	}
    }
    
    public static void showConnectedUsers() {
    	WebsocketServer.connectedUsers.forEach((String sessionid, SocketUser u) -> {
    		System.out.println(u.getUser().getName() + " -> " + sessionid);
    	});
    }
 
    @OnOpen
    public void onOpen(Session session, @PathParam("userid") String userid) throws IOException, EncodeException {
        this.session = session;
        
        SocketUser currentUser = new SocketUser(session.getId(), this, User.fromHex(userid));
        
        //If the old users session mapping still exists, remove it.
        String previousUsersSessionid = connectedUserIdSessionIdMap.get(currentUser.getUser().getId());
        if(previousUsersSessionid != null) {
        	connectedUsers.remove(previousUsersSessionid);
        	
        }
        
        //Setup session maps.
        connectedUsers.put(session.getId(), currentUser);
        connectedUserIdSessionIdMap.put(currentUser.getUser().getId(), session.getId());
        connectedSessions.add(this);
        
        System.out.println(currentUser.getUser().getName() + "<" + currentUser.getUser().getUsername() + "> Connected!");
    }
 
    @OnMessage
    public void onMessage(Session session, Message message) throws IOException, EncodeException {
        switch(getMessageType(message)) {
			case Chat:
				//Prefer to use the sender object here, as it is trustworthy rather than the message sender
				SocketUser sender = connectedUsers.get(session.getId());
				message.setSenderUser(sender.getUser());
				message.getTopic().getGroup();
				
				//Show in server console.
				System.out.println(sender.getUser().getName() + "<" + sender.getUser().getUsername() + "> sent message in '" + message.getTopic().getName() + "' ('" + message.getTopic().getGroup().getName() + "') => " + message.getContent());
				
				//Insert message into pdstore
		        chatStore.insertMessage(message);
		        
		        //Distribute message
		        distribute(message);
				
		        
		        break;
			default:
				Message m = new Message();
				m.setContent("Invalid Message type");
				session.getBasicRemote().sendObject(m);
				break;
        }
    }
 
    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
    	//Get the user object so we can print who left.
    	SocketUser u = connectedUsers.get(session.getId());
    	
    	//Remove the connected session
    	connectedSessions.remove(this);
    	connectedUsers.remove(session.getId());
    	
    	//Print to console who left.
    	System.out.println(u.getUser().getName() + "<" + u.getUser().getUsername() + "> Disconnected!");
    }
 
    @OnError
    public void onError(Session session, Throwable throwable) {
        // Do error handling here
    	System.out.println("Socket Error occoured!");
    	System.out.println(throwable.toString());
    	System.out.println(session.getId());
    }
    
    protected static MessageTypes getMessageType(Message message) {
	    	if(message.getContent().length() > 3 && message.getContent().substring(0, 4).equals("SHOW")) {
	    		return WebsocketServer.MessageTypes.SHOW; 
	    	}
	    	
	    	return WebsocketServer.MessageTypes.Chat;
    }
    
    public static void distribute(Message message) {
    	List<String> messageSentTo = new ArrayList<String>();
    	
    	//Send message notification to all users of the topic who are currently connected via a websocket.
    	List<User> topicMembers = message.getTopic().getMembers();
    	topicMembers.forEach((User u) -> {
    		String sessionid = connectedUserIdSessionIdMap.get(u.getId());
    		if(sessionid != null) {
    			SocketUser connectedUser = connectedUsers.get(sessionid);
				sendMessage(connectedUser, message);
				messageSentTo.add(sessionid);
    		}
    	});
    	
    	//Send message notification to all users who are not subscribed to the topic, but are currently viewing it.
    	List<String> openTopics = currentlyOpenTopics.get(message.getTopic().getId());
    	openTopics.forEach((String sessionid) -> {
    		if(!messageSentTo.contains(sessionid)) {
    			SocketUser connectedUser = connectedUsers.get(sessionid);
        		sendMessage(connectedUser, message);
    		}
    	});
    }
    
    private static void sendMessage(SocketUser user, Message message) {
    	try {
			user.session.session.getBasicRemote().sendObject(message);
		} catch (IOException | EncodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
 
    private static void broadcast(Message message) 
      throws IOException, EncodeException {
  
    	connectedSessions.forEach(endpoint -> {
            synchronized (endpoint) {
                try {
                    endpoint.session.getBasicRemote().sendObject(message);
                } catch (IOException | EncodeException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}