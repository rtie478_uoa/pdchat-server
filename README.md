# PD Chat Server
Server for the PD Chat Application

# Install & Run
1. Clone project to local directory
2. Import the project into the Eclipse IDE. Wait for Eclipse to download all the Maven dependencies.
3. Modify the server IP variables in App.java (for both for the HTTP server and the Web Socket Server).
4. Run App.java

# Credits
- Ryan Tiedemann <rtie478@aucklanduni.ac.nz>